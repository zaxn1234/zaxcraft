#pragma once

#ifndef ZAXCRAFT_FRAMEBUFFER_H
#define ZAXCRAFT_FRAMEBUFFER_H

#include <Core.h>
#include <stdint.h>
#include <cstdint>
#include <Windows.h>

namespace Zaxcraft
{
	struct Texture;
	enum class ByteFormat;

	struct Framebuffer
	{
		unsigned int fbo;
		unsigned int rbo;
		int width;
		int height;

		bool includeDepthStencil;

		std::vector<Texture> colourAttachments;

		void bind() const;
		void unbind() const;
		void clearColourAttachmentUint32(int colourAttachment, unsigned int clearColour) const;
		void clearColourAttachmentRgb(int colourAttachment, glm::vec3 clearColour) const;

		unsigned int readPixelUint32(int colourAttachment, int x, int y) const;
		unsigned int* readAllPixelsRgb8(int colourAttachment) const;
		void freePixelsRgb8(unsigned int* pixels) const;
		const Texture& getColourAttachment(int index) const;

		void regenerate();
		void destroy(bool clearColourAttachmentSpecs = true);
	};

	class FramebufferBuilder
	{
	public:
		FramebufferBuilder(unsigned int width, unsigned int height);
		FramebufferBuilder& includeDepthStencilBuffer();
		Framebuffer generate();

		FramebufferBuilder& addColourAttachment(const Texture& textureSpec);


	private:
		Framebuffer framebuffer;
	};
}

#endif