#pragma once
#ifndef ZAXCRAFT_TEXTURE_H
#define ZAXCRAFT_TEXTURE_H

#include <Core.h>

namespace Zaxcraft
{
	enum class FilterMode
	{
		None = 0,
		Linear,
		LinearMipmapLinear,
		NearestMipmapNearest,
		LinearMipmapNearest,
		NearestMipmapLinear,
		Nearest
	};

	enum class WrapMode
	{
		None = 0,
		Repeat
	};

	enum class ByteFormat
	{
		None = 0,
		RGBA8_UI,
		RGBA_16F,
		RGB8_UI,
		R32_UI,
		R32_F,
		R8_UI,
		R8_F,
		ALPHA_F,
		DepthStencil
	};

	enum class ColourChannel
	{
		None = 0,
		Red,
		Green,
		Blue,
		Alpha,
		Zero,
		One
	};

	enum class TextureType
	{
		None = 0,
		_1D,
		_2D,
		_CUBEMAP,
		_CUBEMAP_POSITIVE_X,
		_CUBEMAP_NEGATIVE_X,
		_CUBEMAP_POSITIVE_Y,
		_CUBEMAP_NEGATIVE_Y,
		_CUBEMAP_POSITIVE_Z,
		_CUBEMAP_NEGATIVE_Z
	};

	struct Texture
	{
		TextureType type;
		unsigned int graphicsId;
		int width;
		int height;

		FilterMode magFilter;
		FilterMode minFilter;
		WrapMode wrapS;
		WrapMode wrapT;

		ByteFormat format;
		ColourChannel swizzleFormat[4];
		
		bool generateMipmap;
		bool generateMipmapFromFile;

		char* path;

		void bind() const;
		void unbind() const;
		void destroy();

		void uploadSubImage(int offsetX, int offsetY, int width, int height, unsigned char* buffer) const;
		bool isNull() const;
	};

	namespace TextureUtil
	{
		unsigned int toGlSizedInternalFormat(ByteFormat format);
		unsigned int toGlExternalFormat(ByteFormat format);
		unsigned int toGl(WrapMode wrapMode);
		unsigned int toGl(FilterMode filterMode);
		unsigned int toGlDataType(ByteFormat format);
		int toGlSwizzle(ColourChannel colorChannel);
		unsigned int toGlType(TextureType type);

		bool byteFormatIsInt(const Texture& texture);
		bool byteFormatIsRgb(const Texture& texture);

		void generateFromFile(Texture& texture);
		void generateEmptyTexture(Texture& texture);
	}

	class TextureBuilder
	{
	public:
		TextureBuilder();
		TextureBuilder(const Texture& texture);

		TextureBuilder& setMagFilter(FilterMode mode);
		TextureBuilder& setMinFilter(FilterMode mode);
		TextureBuilder& setWrapS(WrapMode mode);
		TextureBuilder& setWrapT(WrapMode mode);
		TextureBuilder& setFormat(ByteFormat format);
		TextureBuilder& setFilepath(const char* filepath);
		TextureBuilder& setWidth(unsigned int width);
		TextureBuilder& setHeight(unsigned int height);
		TextureBuilder& setSwizzle(std::initializer_list<ColourChannel> swizzleMask);
		TextureBuilder& setTextureType(TextureType type);
		TextureBuilder& generateTextureObject();
		TextureBuilder& setTextureObject(unsigned int textureObjectId);
		TextureBuilder& generateMipmap();
		TextureBuilder& generateMipmapFromFile();
		TextureBuilder& bindTextureObject();

		Texture generate(bool generateFromFilepath = false);
		Texture build();
	private:
		Texture texture;
	};

}

#endif // !ZAXCRAFT_TEXTURE_H
