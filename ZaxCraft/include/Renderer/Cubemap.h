#pragma once
#ifndef ZAXCRAFT_CUBEMAP_H
#define ZAXCRAFT_CUBEMAP_H

#include <Core.h>
#include <Renderer/Texture.h>

namespace Zaxcraft
{
	struct Shader;

	struct Cubemap
	{
		Texture top;
		Texture bottom;
		Texture left;
		Texture right;
		Texture front;
		Texture back;

		unsigned int graphicsId;
		unsigned int vao;
		unsigned int vbo;

		void render(
			const Shader& shader,
			const glm::mat4& projectionMatrix,
			const glm::mat4& viewMatrix
		) const;

		void destroy();

		static Cubemap generateCubemap(
			const std::string& top,
			const std::string& bottom,
			const std::string& left,
			const std::string& right,
			const std::string& front,
			const std::string& back

		);
	};
}

#endif // !ZAXCRAFT_CUBEMAP_H
