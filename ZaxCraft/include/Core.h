#pragma once

#ifndef ZAXCRAFT_CORE_H
#define ZAXCRAFT_CORE_H

// Glm
#define GLM_EXT_INCLUDED
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/quaternion.hpp>


// Standard includes
#include <algorithm>
#include <array>
#include <bitset>
#include <cstdint>
#include <cstddef>
#include <cstdio>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <future>
#include <iostream>
#include <mutex>
#include <optional>
#include <queue>
#include <random>
#include <stdint.h>
#include <string>
#include <string_view>
#include <thread>
#include <vector>

#include <robin_hood.h>

// Glad && GLFW
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <stb_image.h>
#include <stb_image_write.h>

// Yaml
//#include <yaml-cpp/yaml.h>

// User defined literals
glm::vec4 operator""_hex(const char* hexColor, size_t length);
#endif // !ZAXCRAFT_CORE_H
