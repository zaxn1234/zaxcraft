#pragma once

#ifndef ZAXCRAFT_WINDOW_H
#define ZAXCRAFT_WINDOW_H

#include <Core.h>

namespace Zaxcraft
{

	enum class CursorMode : int
	{
		Hidden,
		Locked,
		Normal
	};

	struct Window
	{
		int width;
		int height;
		const char* title;
		void* windowPtr;

		void makeContextCurrent();
		void pollInput();
		void swapBuffers();
		bool shouldClose();

		void close();
		void destroy();
		void setCursorMode(CursorMode cursorMode);
		void setVSync(bool on);
		void setTitle(const char* title);
		void setSize(int width, int height);
		float getAspectRatio() const;

		static Window* create(const char* title);
		static void init();
		static void free();
	};
}

#endif // !ZAXCRAFT_WINDOW_H
