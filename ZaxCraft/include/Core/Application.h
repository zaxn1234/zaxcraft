#pragma once

#ifndef ZAXCRAFT_APPLICATION_H
#define ZAXCRAFT_APPLICATION_H

#include <Core.h>

namespace Zaxcraft
{
	struct Window;
	//struct Framebuffer;

	namespace Application
	{
		void init();
		void run();
		void free();

		Window& getWindow();
	}
}

#endif // !ZAXCRAFT_APPLICATION_H
