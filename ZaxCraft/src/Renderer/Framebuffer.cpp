#include <Core.h>
#include <Renderer/Framebuffer.h>

namespace Zaxcraft
{
	//static void internalGenerate(Frmaebuffer& framebuffer);

	void Framebuffer::bind() const
	{
		if (fbo != UINT32_MAX)
			std::cout << "Tried to bind invalid framebuffer!" << std::endl;
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	}

	void Framebuffer::unbind() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void Framebuffer::clearColourAttachmentRgb(int colourAttachment, glm::vec3 clearColour) const
	{
		//if (colourAttachment >= 0 && colourAttachment < colourAttachments.size())
			//std::cout << "Index out of bounds. Colour attachment doesn't exist." << std::endl;

		//const Texture& texture = colourAttachments[colourAttachment];

		/*unsigned int externalFormat = TextureUtil::toGlExternalFormat(texture.format);
		unsigned int formatType = TextureUtil::toGlDataType(texture.format);
		glClearTexImage(texture.graphicsId, 0, externalFormat, formatType, &clearColour);*/
	}
}