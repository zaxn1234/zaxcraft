#include <Core.h>
#include <Core/Window.h>
#include <Input/Input.h>

namespace Zaxcraft
{
	void Window::init()
	{
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_SAMPLES, 4);

	}

	static void resizeCallback(GLFWwindow* windowPtr, int newWidth, int newHeight)
	{
		Window* userWindow = (Window*)glfwGetWindowUserPointer(windowPtr);
		userWindow->width = newWidth;
		userWindow->height = newHeight;
		Input::setWindowSize(glm::vec2((float)newWidth, (float)newHeight));
		glViewport(0, 0, newWidth, newHeight);
	}

	Window* Window::create(const char* title)
	{
		Window* res = new Window();

		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		if (!monitor)
		{
			std::cout << "Failed to get primary monitor." << std::endl;
			return nullptr;
		}
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);
		if (!mode)
		{
			std::cout << "Failed to get video mode of primary monitor." << std::endl;
			return nullptr;
		}

		res->width = glm::clamp(mode->width / 2, 300, INT_MAX);
		res->height = glm::clamp(mode->height / 2, 200, INT_MAX);
		res->title = title;

		Input::setWindowSize(glm::vec2((float)res->width, (float)res->height));

		res->windowPtr = (void*)glfwCreateWindow(res->width, res->height, title, nullptr, nullptr);
		if (res->windowPtr == nullptr)
		{
			glfwTerminate();
			std::cout << "Failed to create window." << std::endl;
			return res;
		}
		std::cout << "GLFW window created" << std::endl;


		glfwSetWindowUserPointer((GLFWwindow*)res->windowPtr, (void*)(res));
		res->makeContextCurrent();

		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
		{
			printf("Failed to init Glad.\n");
		}

		glViewport(0, 0, res->width, res->height);

		glfwSetFramebufferSizeCallback((GLFWwindow*)res->windowPtr, resizeCallback);
		glfwSetCursorPosCallback((GLFWwindow*)res->windowPtr, Input::mouseCallback);
		glfwSetKeyCallback((GLFWwindow*)res->windowPtr, Input::keyCallback);
		glfwSetMouseButtonCallback((GLFWwindow*)res->windowPtr, Input::mouseButtonCallback);
		glfwSetCharCallback((GLFWwindow*)res->windowPtr, Input::charCallback);
		glfwSetScrollCallback((GLFWwindow*)res->windowPtr, Input::scrollCallback);

		int monitorX, monitorY;
		glfwGetMonitorPos(monitor, &monitorX, &monitorY);

		int windowWidth, windowHeight;
		glfwGetWindowSize((GLFWwindow*)res->windowPtr, &windowWidth, &windowHeight);

		glfwSetWindowPos((GLFWwindow*)res->windowPtr,
			monitorX + (mode->width - windowWidth) / 2,
			monitorY + (mode->height - windowHeight) / 2);

		res->setVSync(true);

		return res;
	}

	void Window::setTitle(const char* title)
	{
		glfwSetWindowTitle((GLFWwindow*)windowPtr, title);
	}

	void Window::setSize(int width, int height)
	{
		glfwSetWindowSize((GLFWwindow*)windowPtr, width, height);
	}

	void Window::setCursorMode(CursorMode cursorMode)
	{
		int glfwCursorMode =
			cursorMode == CursorMode::Locked ? GLFW_CURSOR_DISABLED :
			cursorMode == CursorMode::Normal ? GLFW_CURSOR_NORMAL :
			cursorMode == CursorMode::Hidden ? GLFW_CURSOR_HIDDEN :
			GLFW_CURSOR_HIDDEN;

		glfwSetInputMode((GLFWwindow*)windowPtr, GLFW_CURSOR, glfwCursorMode);
	}

	void Window::makeContextCurrent()
	{
		glfwMakeContextCurrent((GLFWwindow*)windowPtr);
	}

	void Window::pollInput()
	{
		//Input::endFrame();
		glfwPollEvents();
	}

	void Window::close()
	{
		glfwSetWindowShouldClose((GLFWwindow*)windowPtr, GLFW_TRUE);
	}

	void Window::destroy()
	{
		glfwDestroyWindow((GLFWwindow*)windowPtr);
		windowPtr = nullptr;
	}

	bool Window::shouldClose()
	{
		return glfwWindowShouldClose((GLFWwindow*)windowPtr);
	}

	void Window::swapBuffers()
	{
		glfwSwapBuffers((GLFWwindow*)windowPtr);
	}

	float Window::getAspectRatio() const
	{
		return (float)width / (float)height;
	}

	void Window::setVSync(bool on)
	{
		glfwSwapInterval(on ? 1 : 0);
	}

	void Window::free()
	{
		// Clean up
		glfwTerminate();
	}
}