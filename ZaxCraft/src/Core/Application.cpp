#ifndef STB_IMAGE_IMPLEMENATION
#define STB_IMAGE_IMPLEMENTATION
#endif


#include <Core.h>
#include <Core/Application.h>
#include <Core/Window.h>
#include <Utils/Settings.h>

namespace Zaxcraft
{
	namespace Application
	{
		float deltaTime = 0.016f;

		static void freeWindow();
		static void freeRegistry();

		// Internal variables
		//static Framebuffer mainFramebuffer;
		static bool dumpScreenshot = false;
		static bool screenshotMustBeSquare = false;
		static std::string screenshotName = "";

		//static Shader screenShader;

		void init()
		{
			Window::init();
			Window& window = getWindow();
			if (!window.windowPtr)
			{
				std::cout << "Error: Could not create window." << std::endl;
				return;
			}

		}

		void run()
		{
			// Do things
			Window& window = getWindow();
			double previousTime = glfwGetTime();
			bool inMainMenu = true;
			const float targetFps = 0.016f;
			const float nextTarget = 0.032f;

			float verts[] = {
				 0.5f,  0.5f, 0.0f,  // top right
				 0.5f, -0.5f, 0.0f,  // bottom right
				-0.5f, -0.5f, 0.0f,  // bottom left
				-0.5f,  0.5f, 0.0f   // top left 
			};
			unsigned int indices[] = {  // note that we start from 0!
				0, 1, 3,   // first triangle
				1, 2, 3    // second triangle
			};

			const char* fragmentShaderSource = "#version 460 core\n"
				"out vec4 FragColour;\n"
				"void main()\n"
				"{\n"
				"   FragColour = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
				"}\0";

			const char* vertexShaderSource = "#version 460 core\n"
				"layout (location = 0) in vec3 aPos;\n"
				"void main()\n"
				"{\n"
				"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
				"}\0";

			unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
			glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
			glCompileShader(vertexShader);

			unsigned int fragmentShader;
			fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
			glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
			glCompileShader(fragmentShader);

			int  successVert;
			char infoLogVert[512];
			glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &successVert);

			if (!successVert) {
				printf("Failed compiling vertex shader!");
				glGetShaderInfoLog(vertexShader, 512, NULL, infoLogVert);
				std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLogVert << std::endl;
			}

			int  successFrag;
			char infoLogFrag[512];
			glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &successFrag);

			if (!successFrag) {
				printf("Failed compiling frag shader!");
				glGetShaderInfoLog(fragmentShader, 512, NULL, infoLogFrag);
				std::cout << "ERROR::SHADER::FRAG::COMPILATION_FAILED\n" << infoLogFrag << std::endl;
			}

			unsigned int shaderProgram = glCreateProgram();
			glAttachShader(shaderProgram, vertexShader);
			glAttachShader(shaderProgram, fragmentShader);
			glLinkProgram(shaderProgram);

			int successProgram;
			char infoLogProg[512];
			glGetProgramiv(shaderProgram, GL_LINK_STATUS, &successProgram);
			if (!successProgram) {
				printf("Failed linking shader program!");
				glGetProgramInfoLog(shaderProgram, 512, NULL, infoLogProg);
				std::cout << "ERROR::SHADER::PROGRAM::COMPILATION_FAILED\n" << infoLogProg << std::endl;
			}

			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
			glEnableVertexAttribArray(0);

			glUseProgram(shaderProgram);
			glDeleteShader(vertexShader);
			glDeleteShader(fragmentShader);

			unsigned int EBO;
			glGenBuffers(1, &EBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

			// ..:: Initialization code (done once (unless your object frequently changes)) :: ..
			// 1. bind Vertex Array Object
			unsigned int VAO;
			glGenVertexArrays(1, &VAO);
			glBindVertexArray(VAO);
			// 2. copy our vertices array in a buffer for OpenGL to use
			unsigned int VBO;
			glGenBuffers(1, &VBO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
			// 3. then set our vertex attributes pointers
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
			glEnableVertexAttribArray(0);
			
			while (!window.shouldClose())
			{
				double currentTime = glfwGetTime();
				deltaTime = (float)(currentTime - previousTime);

				/*const GLenum mainDrawBuffer[3] = {
					GL_COLOR_ATTACHMENT0, GL_NONE, GL_NONE
				};
				glDrawBuffers(3, mainDrawBuffer);

				const float zeroFillerVec[4] = {
					0.0f, 0.0f, 0.0f, 1.0f
				};
				glClearBufferfv(GL_COLOR, 0, zeroFillerVec);

				float one = 1;
				glClearBufferfv(GL_DEPTH, 0, &one);
*/
				glDisable(GL_DEPTH_TEST);
				glDepthMask(GL_TRUE);
				glDisable(GL_BLEND);

				glClearColor(0.0f, 0.5f, 1.0f, 1.0f);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
				glEnable(GL_DEPTH_TEST);


				glUseProgram(shaderProgram);
				glBindVertexArray(VAO);
				//glDrawArrays(GL_TRIANGLES, 0, 3);

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
				glBindVertexArray(0);
				
				window.swapBuffers();
				window.pollInput();
			}
		}

		void free()
		{
			Window& window = getWindow();
			window.destroy();

			Window::free();
			//freeWindow();
		}

		Window& getWindow()
		{
			static Window* window = Window::create(Settings::Window::title);
			return *window;
		}
	}
}