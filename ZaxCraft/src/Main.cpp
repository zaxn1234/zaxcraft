/*
 * ZaxCraft - Minecraft clone
 * 
 * Izaak Webster (c) 2021
 */

#include <Core.h>
#include <Core/Application.h>

int main(int argc, char** argv[])
{
	std::cout << "Hello, world!" << std::endl;

	Zaxcraft::Application::init();
	Zaxcraft::Application::run();
	Zaxcraft::Application::free();

	return 0;
}